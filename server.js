var express = require('express');
var app = express();
var morgan = require('morgan');
const path = require('path');
const bodyparser = require('body-parser')
require('dotenv').config();
var compression = require('compression');
var router = express.Router();
 var rootRouter  = require('./src/routes/index.js')(router);
var cors = require('cors');


/**
 * Middlewares go here for the application.
 * if it gets to cumbersome then we can move to seperate file
 * if it gets to cumbersome then we can move to seperate file
 * 
 */

//setting view engine 
app.set('view engine', 'hbs')


app.use(compression());
app.use(morgan('dev'));
app.use(bodyparser.urlencoded({ extended: true }));
app.use(bodyparser.json());
// Using static files
app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/output'));
app.set('views', path.join(__dirname, 'views'))
app.use(express.json());//for parsing application/json
app.use(express.urlencoded({ extended: false})); //for parsing application/x-www-form-urlencoded
app.use(cors());
 app.use('/api', rootRouter);

app.get('/', (req, res) => {
    res.render('index')
})



app.use((req, res, next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
})
app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
            error: {
                    message: error.message
            }
    })
})



app.all('*', (req, res) => res.status(200).send({message : 'server is live'}));

module.exports = app;