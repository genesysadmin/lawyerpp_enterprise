const model = require('../models/court/court');

exports.createCourt = (options) => {
    return new Promise((resolve, reject) => {
        let details = {
            name_Of_court: options.name_Of_court,
            judicial_division: options.judicial_division,
            courts_designations: [{
                court_designation: options.court_designation
            }],
            judges: [{
                judge_id: options.judge_id,
                year_of_appointment_to_court: options.year_of_appointment_to_court,
                judge_role: options.judge_role,
                judge_administrative_right: options.judge_administrative_right,
                judge_court_designation: options.judge_court_designation,
            }],
            court_staff: [{
                court_staff_id: options.court_staff_id,
                year_of_employment: options.year_of_employment,
                staff_role: options.staff_role,
                staff_administrative_right: options.staff_administrative_right,
                staff_court_designation: options.staff_court_designation,


            }]
        }
        model.findOne({ name_Of_court: options.name_Of_court, judicial_division: options.judicial_division }).exec((err, exists) => {
            if (err) reject({ success: false, err: err, status: 500 })
            if (exists) {
                resolve({ success: false, message: 'court already exists', status: 401 })
            } else {
                model.create(details).then(created => {
                    if (created) {
                        resolve({ success: true, message: 'court created successfully !!!', status: 200 })
                    } else {
                        resolve({ success: false, message: 'Error creating court !!!', status: 401 })
                    }
                }).catch(err => reject({ err: err.message, status: 500 }))
            }
        })
    })
}

exports.getAllCourt = (pagenumber = 1, pagesize = 20)=>{
    return new Promise((resolve , reject)=>{
        model.find({})
        .skip((parseInt(pagenumber) - 1) * parseInt(pagesize))
        .limit(parseInt(pagesize))
        .populate({ path: "court_staff.court_staff_id", model: 'client', select: { __v: 0, password: 0, status_code: 0 } })
        .populate({ path: "judges.judge_id", model: 'lawyer', select: { __v: 0, password: 0, status_code: 0 } })
        .exec((err, result)=>{
            if (err) reject({ success: false, err: err, status: 500 })
            if(result){
                resolve({ success: true, message: 'court list!!!', data:result , status: 200 }) 
            }else{
                resolve({ success: false, message: 'could not find court!!!', status: 404 })
            }
        })
    })
}

exports.getCourtById = (courtId)=>{
    return new Promise((resolve , reject)=>{
        model.findById({_id:courtId})
        .populate({ path: "court_staff.court_staff_id", model: 'client', select: { __v: 0, password: 0, status_code: 0 } })
        .populate({ path: "judges.judge_id", model: 'lawyer', select: { __v: 0, password: 0, status_code: 0 } })
        .exec((err, result)=>{
            if (err) reject({ success: false, err: err, status: 500 })
            if(result){
                resolve({ success: true, message: 'court details!!!', data:result , status: 200 }) 
            }else{
                resolve({ success: false, message: 'could not find court!!!', status: 404 })
            }
        })
    })
}