const model = require('../models/corporateOrg/corporateOrg');
const corporateOrgAdmin = require('../models/corporateOrg/corporateOrgAdmin')
const corporateOrgEmployee = require('../models/corporateOrg/corporateOrgEmployees')
const user = require('../models/users/users');
const formatter = require('../utils/userFormatter');
exports.createCorporateOrg = (userId, option) => {
    return new Promise((resolve, reject) => {
        user.findOne({ public_id: userId }).exec((err, exists) => {
            if (err) reject({ success: false, err: err, status: 500 })
            if (exists) {
                const details = {
                    Name_of_organisation: option.Name_of_organisation,
                    Email_address: option.Email_address,
                    Website: option.Website,
                    Contact_number: option.Contact_number,
                    Contact_address: option.Contact_address,
                    OwnerId: exists._id,
                    userType: exists.user_type,
                    Type_of_Organisation: option.Type_of_Organisation,
                    Type_of_Government_MDA: option.Type_of_Government_MDA
                }
                model.findOne({
                    OwnerId: exists._id, Name_of_organisation: option.Name_of_organisation, Contact_address: option.Contact_number
                    , Contact_address: option.Contact_address
                }).exec((err, result) => {
                    if (err) reject({ success: false, err: err, status: 500 })
                    if (result == null || result.lenght == 0) {
                        model.create(details).then(created => {
                            if (created) {
                                let orgDetails = {
                                    corporateOrgId: created._id,
                                    adminId: exists._id
                                }
                                corporateOrgAdmin.findOne({ corporateOrgId: orgDetails.corporateOrgId, adminId: orgDetails.adminId }).exec((err, found) => {
                                    if (err) reject({ success: false, err: err, status: 500 })
                                    if (found == null || found.lenght == 0) {
                                        corporateOrgAdmin.create(orgDetails).then(added => {
                                            added ? resolve({ success: true, message: 'Corporate organisation created', status: 200 }) :
                                                resolve({ success: false, message: 'Error creating corporate organisation', status: 404 })

                                        }).catch(err => reject({ err: err, status: 500 }))
                                    } else {
                                        resolve({ success: false, message: "Sorry user is already and admin !!", status: 401 })
                                    }
                                })

                            } else {
                                resolve({ success: false, message: 'Error creating corporate organisation', status: 404 })

                            }
                        }).catch(err => reject({ err: err, status: 500 }))
                    } else {
                        resolve({ success: false, message: 'corporate organisation already Exists !!!', status: 404 })

                    }
                })
            } else {
                resolve({ success: false, message: 'user does not exist', status: 404 })

            }
        })
    })
}

exports.AddCorporateOrgEmployees = (id, lawyer, orgId) => {
    return new Promise((resolve, reject) => {
        user.findOne({ public_id: id }).exec((err, exists) => {
            if (err) reject({ success: false, err: err, status: 500 })
            exists == null ? resolve({ success: false, message: "user does not exist", status: 400 }) :
                corporateOrgAdmin.findOne({ adminId: exists._id, corporateOrgId: orgId.organisationId }).exec((err, exist) => {
                    if (err) reject({ success: false, err: err, status: 500 })
                    if (exist == null) {
                        resolve({ success: false, message: "Sorry you are not authorised to carry out this command ", status: 404 })
                    } else {
                        formatter.getLawyerId(lawyer).then(available => {
                            if (available) {
                                let detail = {
                                    employeeId: available,
                                    corporateOrgId: orgId.organisationId
                                }
                                corporateOrgEmployee.findOne({ corporateOrgId: orgId.organisationId, employeeId: available }).exec((err, exist) => {
                                    if (err) reject({ success: false, err: err, status: 500 })
                                    if (exist == null) {
                                        corporateOrgEmployee.create(detail).then(add => {
                                            add ? resolve({ success: true, message: 'employee added successfully', status: 200 }) :
                                                resolve({ success: false, message: available.message, status: 401 })
                                        }).catch(err => reject({ err: err, status: 500 }))
                                    }
                                })
                            } else {
                                resolve({ success: false, message: available.message, status: 404 })
                            }
                        }).catch(err => reject({ err: err.message, status: 500 }))
                    }
                })

        })

    })
}

exports.getAllUserOrganisation = (id, pagenumber = 1, pagesize = 20) => {
    return new Promise((resolve, reject) => {
        user.findOne({ public_id: id }).exec((err, res) => {
            if (err) reject({ success: false, err: err, status: 500 })
            if (res) {
                model.find({ OwnerId: res._id })
                    .skip((parseInt(pagenumber) - 1) * parseInt(pagesize))
                    .limit(parseInt(pagesize))
                    .populate({ path: "OwnerId", model: 'users', select: { __v: 0, password: 0, status_code: 0 } })
                    .exec((err, response) => {
                        if (err) reject({ success: false, err: err, status: 500 })
                        if (response) {
                            resolve({ success: true, message: 'user organisation', data: response, status: 200 })
                        } else {
                            resolve({ success: false, message: 'unable to get user organisation', status: 404 })
                        }
                    })
            } else {
                resolve({ success: false, message: 'user does not exist', status: 404 })
            }
        })

    })
}

exports.getOrgAdmins = (id, pagenumber = 1, pagesize = 20) => {
    return new Promise((resolve, reject) => {
        corporateOrgAdmin.findOne({ corporateOrgId: id })
            .skip((parseInt(pagenumber) - 1) * parseInt(pagesize))
            .limit(parseInt(pagesize))
            .populate({ path: "corporateOrgId", model: 'corporateOrg', select: { __v: 0, password: 0, status_code: 0 } })
            .populate({ path: "adminId", model: 'users', select: { __v: 0, password: 0, status_code: 0 } })
            .exec((err, result) => {
                console.log(result, 'hmmmmmm')

                if (err) reject({ success: false, err: err, status: 500 })
                if (result) {
                    resolve({ success: true, message: "Admin list", data: result, status: 200 })
                } else {
                    resolve({ success: false, message: "could not find admin list", status: 404 })
                }
            })
    })
}

exports.getOrgEmployees = (id, pagenumber = 1, pagesize = 20) => {
    return new Promise((resolve, reject) => {
        corporateOrgEmployee.findOne({ corporateOrgId: id })
            .skip((parseInt(pagenumber) - 1) * parseInt(pagesize))
            .limit(parseInt(pagesize))
            .populate({ path: "corporateOrgId", model: 'corporateOrg', select: { __v: 0, password: 0, status_code: 0 } })
            .populate({ path: "employeeId", model: 'lawyer', select: { __v: 0, password: 0, status_code: 0 } })
            .exec((err, result) => {
                if (err) reject({ success: false, err: err, status: 500 })
                if (result) {
                    resolve({ success: true, message: "Employee  list", data: result, status: 200 })
                } else {
                    resolve({ success: false, message: "could not find employee list", status: 404 })
                }
            })
    })
}

exports.AddOrganisationAdmin = (orgId,adminId, data) => {
    return new Promise((resolve, reject) => {
        corporateOrgAdmin.findOne({ corporateOrgId: orgId, adminId: adminId }).exec((err, result) => {
            if (err) reject({ success: false, err: err, status: 500 })
            if (result) {
                formatter.getLawyerId(data.userId).then(found => {
                    if (found) {
                        corporateOrgEmployee.findOne({ employeeId: found, corporateOrgId: orgId }).exec((err, res) => {
                            if (err) reject({ success: false, err: err, status: 500 })
                            if (res) {
                                corporateOrgAdmin.findOne({ corporateOrgId: orgId, adminId: data.userId }).exec((err, done) => {
                                    if (err) reject({ success: false, err: err, status: 500 })
                                    if (done) {
                                        resolve({ success: false, message: 'user is already and admin', status: 401 })
                                    } else {
                                        let orgDetails = {
                                            corporateOrgId: orgId,
                                            adminId: data.userId
                                        }
                                        corporateOrgAdmin.create(orgDetails).then(create => {
                                            create ? resolve({ success: true, message: "employee has been added as an admin ", status: 200 }) :
                                                resolve({ success: false, message: "Error adding employee as admin ", status: 400 });
                                        })
                                    }
                                })
                            } else {
                                resolve({ success: false, message: "this user is not and employee in this organisation and cant be and admin", status: 401 })
                            }
                        })
                    } else {
                        resolve({ success: false, message: found.message, status: 404 })
                    }
                }).catch(err => reject({ err: err.message, status: 500 }))

            } else {
                resolve({ success: false, message: "user is not an admin to this organisation", status: 401 })

            }
        })
    })
}

exports.getEmployeeOrganisation = (id)=>{
    return new Promise((resolve ,reject)=>{
        formatter.getLawyerId(id).then(result =>{
            if(result){
                corporateOrgEmployee.find({employeeId:result})
                .populate({ path: "corporateOrgId", model: 'corporateOrg', select: { __v: 0, password: 0, status_code: 0 } })
                .populate({ path: "employeeId", model: 'lawyer', select: { __v: 0, password: 0, status_code: 0 } })
                .exec((err, res)=>{
                    if (err) reject({ success: false, err: err, status: 500 })
                    if(res){
                        resolve({success:true , message:"Employee organisations" , data:res , status:200})
                    }else{
                        resolve({success:false , message:"No organisation for this employee" , status:404})
                    }
                })
            }else{
                resolve({success:false , message:"Lawyer does nit exist", status:404})
            }
        }).catch(err => reject({ err: err.message, status: 500 }))
    })
}

exports.deleteEmployeeFromOrganisation = (orgId ,userId , employeeId)=>{
    return new Promise((resolve ,reject)=>{
        corporateOrgAdmin.findOne({ corporateOrgId: orgId, adminId: userId }).exec((err, result) => {
            if (err) reject({ success: false, err: err, status: 500 })
            if(result){
                formatter.getLawyerId(employeeId).then(exists =>{
                    if(exists){
                        corporateOrgEmployee.findOneAndRemove({corporateOrgId:orgId ,employeeId:exists}).exec((err , deleted)=>{
                            if (err) reject({ success: false, err: err, status: 500 })
                            if(deleted){
                                resolve({success:true , message:"employee has been deleted from this organisation", status:200})  
                            }else{
                                resolve({success:false , message:"Error deleting employee", status:400})  
                            }
                        })
                    }else{
                        resolve({success:false , message:"lawyer does not exist ", status:400})  
                    }
                }).catch(err => reject({ err: err.message, status: 500 }))
          
            }else{
                resolve({success:false , message:"user is not an admin", status:400})
            }
        })

    })
}

exports.organsisationsMatters = (userId)=>{
    return new Promise((resolve , reject)=>{
        
    })
}