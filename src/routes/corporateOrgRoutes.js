const router = require('express').Router()
const corporateOrgController = require('../controllers/corporateOrgController')
const middleware = require('../middlewares/authMiddleware')
module.exports = function(){
    const corpOrgCtrl = new corporateOrgController()

    router.post('/create', middleware.authenticate, corpOrgCtrl.createCorporateOrg)
    router.post('/add_admin', middleware.authenticate, corpOrgCtrl.AddOrganisationAdmin)
    router.post('/add_employee', middleware.authenticate, corpOrgCtrl.AddCorporateOrgEmployees)
    router.get('/user_orgs/:pagesize/:pagenumber', middleware.authenticate, corpOrgCtrl.getAllUserOrganisation)
    router.get('/org-admin/:pagesize/:pagenumber', middleware.authenticate, corpOrgCtrl.getOrgAdmins)
    router.get('/org-employee/:pagesize/:pagenumber', middleware.authenticate, corpOrgCtrl.getOrgEmployees)
    router.get('/employee_org', middleware.authenticate, corpOrgCtrl.getEmployeeOrganisation)
    router.delete('/delete_employee', middleware.authenticate, corpOrgCtrl.deleteEmployeeFromOrganisation)
    



    return router;
}