const corpOrgRoutes = require('./corporateOrgRoutes')
const courtRoutes = require('./courtRoutes')
module.exports = (router) => {
   router.use('/corporg', corpOrgRoutes());
   router.use('/court', courtRoutes());
   return router;
}