const router = require('express').Router()
const courtController = require('../controllers/courtController')
const middleware = require('../middlewares/authMiddleware')
module.exports = function(){
    const courtCtrl = new courtController()

    router.post('/create', courtCtrl.createCourt) 
    router.get('/all_courts/:pagesize/:pagenumber', courtCtrl.getAllCourt) 
    router.get('/single_court', courtCtrl.getCourtById) 
    return router;
}