
const mongoose = require('mongoose');
const schema = mongoose.Schema;

var corporateOrgAdminSchema = new schema({
    corporateOrgId:{ type:mongoose.SchemaTypes.ObjectId, ref: 'corporateOrg', autopopulate: true },
    adminId:{ type:mongoose.SchemaTypes.ObjectId, ref: 'users', autopopulate: true },
    softDelete:{type:Boolean , default: false },
}, {timestamps : { currentTime : () => Math.floor(Date.now() / 1000)}})

module.exports = mongoose.model('corporateOrgAdmin', corporateOrgAdminSchema)
