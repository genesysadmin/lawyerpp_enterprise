
const mongoose = require('mongoose');
const schema = mongoose.Schema;

var corporateOrgEmployeeSchema = new schema({
    corporateOrgId:{ type:mongoose.SchemaTypes.ObjectId, ref: 'corporateOrg', autopopulate: true },
    employeeId:{ type:mongoose.SchemaTypes.ObjectId, ref: 'lawyer', autopopulate: true },
    softDelete:{type:Boolean , default: false },
}, {timestamps : { currentTime : () => Math.floor(Date.now() / 1000)}})

module.exports = mongoose.model('corporateOrgEmployee', corporateOrgEmployeeSchema)
