
const mongoose = require('mongoose');
const schema = mongoose.Schema;

var corporateOrgSchema = new schema({
    Name_of_organisation: {type: String, required:true},
    Email_address:{type: String, required:true},
    Website:{type: String, required:true},
    Contact_number:{type: String, required:true},
    Contact_address:{type: String, required:true},
    OwnerId:{ type:mongoose.SchemaTypes.ObjectId, ref: 'users', autopopulate: true },
    userType:{type:String},
    Type_of_Organisation: {type: String, required:true},
    Type_of_Government_MDA: {type: String, required:true},
    softDelete:{type:Boolean , default: false },
}, {timestamps : { currentTime : () => Math.floor(Date.now() / 1000)}})

module.exports = mongoose.model('corporateOrg', corporateOrgSchema)
