const mongoose = require('mongoose');
const schema = mongoose.Schema;
const courtSchema = new schema({
    name_Of_court:{type:String , required:true},
    judicial_division:{type:String , required:true},
    courts_designations:[{
        court_designation: {type:String , required:true}
    }],
    judges:[{
      judge_id:{ type:mongoose.SchemaTypes.ObjectId, ref: 'lawyer', autopopulate: true },
      year_of_appointment_to_court:{type:Date , required:true},
      judge_role:{type:String , required:true},
      judge_administrative_right:{type:String , required:true},
      judge_court_designation: {type:String , required:true},

    }],
    court_staff:[{
        court_staff_id:{ type:mongoose.SchemaTypes.ObjectId, ref: 'client', autopopulate: true },
        year_of_employment:{type:String , required:true},
        staff_role:{type:String, required:true},
        staff_administrative_right:{type:String , required:true},
        staff_court_designation: {type:String , required:true},


    }]
},{timestamps : { currentTime : () => Math.floor(Date.now() / 1000)}})

module.exports = mongoose.model('court', courtSchema)