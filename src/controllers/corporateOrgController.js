const service = require('../services/corporateOrgService');
module.exports = function corporateOrgController(){
    this.createCorporateOrg = (req,res)=>{
        service.createCorporateOrg(req.auth.publicId , req.body).then(data =>{
            res.status(data.status).send(data);
        }).catch(err => res.status(err.status).send(err))
    }

    this.AddCorporateOrgEmployees = (req,res)=>{
        service.AddCorporateOrgEmployees(req.auth.publicId ,req.query.lawyerId, req.body).then(data =>{
            res.status(data.status).send(data);
        }).catch(err => res.status(err.status).send(err))
    }

    this.getAllUserOrganisation = (req,res)=>{
        service.getAllUserOrganisation(req.auth.publicId,req.params.pagenumber, req.params.pagesize).then(data =>{
            res.status(data.status).send(data);
        }).catch(err => res.status(err.status).send(err))
    }

    this.getOrgAdmins = (req,res)=>{
        service.getOrgAdmins(req.query.orgId,req.params.pagenumber, req.params.pagesize).then(data =>{
            res.status(data.status).send(data);
        }).catch(err => res.status(err.status).send(err))
    }

    this.getOrgEmployees = (req,res)=>{
        service.getOrgEmployees(req.query.orgId,req.params.pagenumber, req.params.pagesize).then(data =>{
            res.status(data.status).send(data);
        }).catch(err => res.status(err.status).send(err))
    }

    this.AddOrganisationAdmin = (req,res)=>{
        service.AddOrganisationAdmin(req.query.orgId, req.auth.Id ,req.body).then(data =>{
            res.status(data.status).send(data);
        }).catch(err => res.status(err.status).send(err))
    }

    this.getEmployeeOrganisation = (req,res)=>{
        service.getEmployeeOrganisation(req.query.lawyerId).then(data =>{
            res.status(data.status).send(data);
        }).catch(err => res.status(err.status).send(err))
    }

    this.deleteEmployeeFromOrganisation = (req,res)=>{
        service.deleteEmployeeFromOrganisation(req.query.orgId ,req.auth.Id , req.body.lawyerId).then(data =>{
            res.status(data.status).send(data);
        }).catch(err => res.status(err.status).send(err))
    }
}