const service = require('../services/courtService');

module.exports = function courtController (){
    this.createCourt = (req,res)=>{
        service.createCourt(req.body).then(data =>{
            res.status(data.status).send(data);
        }).catch(err => res.status(err.status).send(err))
    }

    this.getAllCourt = (req,res)=>{
        service.getAllCourt(req.params.pagenumber, req.params.pagesize).then(data =>{
            res.status(data.status).send(data);
        }).catch(err => res.status(err.status).send(err))
    }

    this.getCourtById = (req,res)=>{
        service.getCourtById(req.query.courtId).then(data =>{
            res.status(data.status).send(data);
        }).catch(err => res.status(err.status).send(err))
    }
}